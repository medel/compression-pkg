MAKETOP = $(realpath .)
include Makefile.env

TARGET = compression 

SRCS = 
SRCS += main.c
SRCS += render_facemesh.c
SRCS += tflite_facemesh.cpp
SRCS += $(MAKETOP)/common/assertgl.c
SRCS += $(MAKETOP)/common/assertegl.c
SRCS += $(MAKETOP)/common/util_egl.c
SRCS += $(MAKETOP)/common/util_shader.c
SRCS += $(MAKETOP)/common/util_matrix.c
SRCS += $(MAKETOP)/common/util_texture.c
SRCS += $(MAKETOP)/common/util_render2d.c
SRCS += $(MAKETOP)/common/util_debugstr.c
SRCS += $(MAKETOP)/common/util_pmeter.c
SRCS += $(MAKETOP)/common/util_tflite.cpp
SRCS += $(MAKETOP)/common/winsys/$(WINSYS_SRC).c

OBJS += $(patsubst %.cc,%.o,$(patsubst %.cpp,%.o,$(patsubst %.c,%.o,$(SRCS))))

LDFLAGS  +=
LIBS     += -pthread

# for V4L2 camera capture
CFLAGS   += -DUSE_INPUT_CAMERA_CAPTURE
CFLAGS   += -DUSE_INPUT_CAMERA_CAPTURE2
SRCS     += $(MAKETOP)/common/util_camera_capture.c
SRCS     += $(MAKETOP)/common/util_v4l2.c
SRCS	 += $(MAKETOP)/common/util_libcamera.cpp
CFLAGS += $(shell pkg-config --cflags libcamera)
LIBS   += $(shell pkg-config --libs   libcamera)
SRCS     += $(MAKETOP)/common/util_drm.c
LIBS     += -ldrm

GST_LIBS= gstreamer-1.0 \
	  gstreamer-app-1.0
CFLAGS += $(shell pkg-config --cflags $(GST_LIBS))
LIBS   += $(shell pkg-config --libs   $(GST_LIBS))

# ---------------------
#  for ImGui
# ---------------------
ENABLE_IMGUI = true
ifeq ($(ENABLE_IMGUI), true)
CFLAGS   += -DUSE_IMGUI
CFLAGS   += -DIMGUI_IMPL_OPENGL_ES2

SRCS += render_imgui.cpp
SRCS += $(MAKETOP)/third_party/imgui/imgui.cpp
SRCS += $(MAKETOP)/third_party/imgui/imgui_draw.cpp
SRCS += $(MAKETOP)/third_party/imgui/imgui_widgets.cpp
SRCS += $(MAKETOP)/third_party/imgui/examples/imgui_impl_opengl3.cpp

INCLUDES += -I$(MAKETOP)/third_party/imgui
INCLUDES += -I$(MAKETOP)/third_party/imgui/examples
endif


# ---------------------
#  for TFLite
# ---------------------
TENSORFLOW_DIR = $(HOME)/work/tensorflow

INCLUDES += -I$(TENSORFLOW_DIR)
INCLUDES += -I$(TENSORFLOW_DIR)/tensorflow/lite/tools/make/downloads/flatbuffers/include
INCLUDES += -I$(TENSORFLOW_DIR)/tensorflow/lite/tools/make/downloads/absl
INCLUDES += -I$(TENSORFLOW_DIR)/external/flatbuffers/include
INCLUDES += -I$(TENSORFLOW_DIR)/external/com_google_absl

LDFLAGS  += -Wl,--allow-multiple-definition

include Makefile.include
