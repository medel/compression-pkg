# Compression

This demo is based on the awesome
[tflite_gles_app](https://github.com/terryky/tflite_gles_app)
repository which mainly allows us to execute tensorflow lite
models outside of the mediapipe framework, and comes with
additional features including support for complex cameras
through libcamera, GStreamer integration, adapted
visualization of the results for our compression demo, and
improved landmark and detection model.



