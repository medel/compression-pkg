/* ------------------------------------------------ *
 * The MIT License (MIT)
 * Copyright (c) 2019 terryky1220@gmail.com
 * ------------------------------------------------ */
#ifndef CAMERA_CAPTURE_H_
#define CAMERA_CAPTURE_H_

#include <stdint.h>
#include <linux/videodev2.h>

#define CAPTURE_SQUARED_CROP        (1 << 0)
#define CAPTURE_PIXFORMAT_RGBA      (1 << 1)

typedef struct _capture_frame_t
{
    int     bo_handle;
    int     prime_fd;
    unsigned int     index;
    void    *vaddr;

    struct v4l2_buffer v4l_buf;

} capture_frame_t;

typedef struct _capture_stream_t
{
    unsigned int    memtype;
    unsigned int    buftype;
    int             bufcount;
    capture_frame_t *frames;
    struct v4l2_format format;
} capture_stream_t;

typedef struct _capture_dev_t
{
    int              v4l_fd;
    char             dev_name[64];
    unsigned int     dev_type;
    capture_stream_t stream;
} capture_dev_t;

int init_capture (uint32_t flags, int stream_width, int stream_height);
int get_capture_dimension (int *width, int *height);
int get_capture_pixformat (uint32_t *pixformat);
int get_capture_buffer (void ** buf);

int start_capture ();

#endif
